<!-- vim-markdown-toc GFM -->

* [ex1:冰墩墩](#ex1冰墩墩)
* [ex2:模拟用户登陆](#ex2模拟用户登陆)
* [ex3:狐狸洞](#ex3狐狸洞)
* [ex4:三维向量及其运算](#ex4三维向量及其运算)
* [ex5:密码强度和正则表达](#ex5密码强度和正则表达)
* [ex6:计算机](#ex6计算机)
* [ex7:961位院士信息爬取](#ex7961位院士信息爬取)
* [ex8:龟兔赛跑图](#ex8龟兔赛跑图)

<!-- vim-markdown-toc -->

🌷 support copy button on top-right corner code area

## ex1:冰墩墩

```python
import turtle
turtle.title('PythonBingDwenDwen')
turtle.speed(100)  # 速度

# 左手
turtle.penup()
turtle.goto(177, 112)
turtle.pencolor("lightgray")
turtle.pensize(3)
turtle.fillcolor("white")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(80)
turtle.circle(-45, 200)
turtle.circle(-300, 23)
turtle.end_fill()

# 左手内
turtle.penup()
turtle.goto(182, 95)
turtle.pencolor("black")
turtle.pensize(1)
turtle.fillcolor("black")
turtle.begin_fill()
turtle.setheading(95)
turtle.pendown()
turtle.circle(-37, 160)
turtle.circle(-20, 50)
turtle.circle(-200, 30)
turtle.end_fill()


# 轮廓
# 头顶
turtle.penup()
turtle.goto(-73, 230)
turtle.pencolor("lightgray")
turtle.pensize(3)
turtle.fillcolor("white")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(20)
turtle.circle(-250, 35)

# 左耳
turtle.setheading(50)
turtle.circle(-42, 180)

# 左侧
turtle.setheading(-50)
turtle.circle(-190, 30)
turtle.circle(-320, 45)

# 左腿
turtle.circle(120, 30)
turtle.circle(200, 12)
turtle.circle(-18, 85)
turtle.circle(-180, 23)
turtle.circle(-20, 110)
turtle.circle(15, 115)
turtle.circle(100, 12)

# 右腿
turtle.circle(15, 120)
turtle.circle(-15, 110)
turtle.circle(-150, 30)
turtle.circle(-15, 70)
turtle.circle(-150, 10)
turtle.circle(200, 35)

turtle.circle(-150, 20)

# 右手
turtle.setheading(-120)
turtle.circle(50, 30)
turtle.circle(-35, 200)
turtle.circle(-300, 23)

# 右侧
turtle.setheading(86)
turtle.circle(-300, 26)

# 右耳
turtle.setheading(122)
turtle.circle(-53, 160)
turtle.end_fill()

# 右耳内
turtle.penup()
turtle.goto(-130, 180)
turtle.pencolor("black")
turtle.pensize(1)
turtle.fillcolor("black")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(120)
turtle.circle(-28, 160)
turtle.setheading(210)
turtle.circle(150, 20)
turtle.end_fill()

# 左耳内
turtle.penup()
turtle.goto(90, 230)
turtle.setheading(40)
turtle.begin_fill()
turtle.pendown()
turtle.circle(-30, 170)
turtle.setheading(125)
turtle.circle(150, 23)
turtle.end_fill()

# 右手内
turtle.penup()
turtle.goto(-180, -55)
turtle.fillcolor("black")
turtle.begin_fill()
turtle.setheading(-120)
turtle.pendown()
turtle.circle(50, 30)
turtle.circle(-27, 200)
turtle.circle(-300, 20)
turtle.setheading(-90)
turtle.circle(300, 14)
turtle.end_fill()

# 左腿内
turtle.penup()
turtle.goto(108, -168)
turtle.fillcolor("black")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(-115)
turtle.circle(110, 15)
turtle.circle(200, 10)
turtle.circle(-18, 80)
turtle.circle(-180, 13)
turtle.circle(-20, 90)
turtle.circle(15, 60)
turtle.setheading(42)
turtle.circle(-200, 29)
turtle.end_fill()

# 右腿内
turtle.penup()
turtle.goto(-38, -210)
turtle.fillcolor("black")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(-155)
turtle.circle(15, 100)
turtle.circle(-10, 110)
turtle.circle(-100, 30)
turtle.circle(-15, 65)
turtle.circle(-100, 10)
turtle.circle(200, 15)
turtle.setheading(-14)
turtle.circle(-200, 27)
turtle.end_fill()


# 右眼
# 眼圈
turtle.penup()
turtle.goto(-64, 120)
turtle.begin_fill()
turtle.pendown()
turtle.setheading(40)
turtle.circle(-35, 152)
turtle.circle(-100, 50)
turtle.circle(-35, 130)
turtle.circle(-100, 50)
turtle.end_fill()

# 眼珠
turtle.penup()
turtle.goto(-47, 55)
turtle.fillcolor("white")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(0)
turtle.circle(25, 360)
turtle.end_fill()
turtle.penup()
turtle.goto(-45, 62)
turtle.pencolor("darkslategray")
turtle.fillcolor("darkslategray")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(0)
turtle.circle(19, 360)
turtle.end_fill()
turtle.penup()
turtle.goto(-45, 68)
turtle.fillcolor("black")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(0)
turtle.circle(10, 360)
turtle.end_fill()
turtle.penup()
turtle.goto(-47, 86)
turtle.pencolor("white")
turtle.fillcolor("white")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(0)
turtle.circle(5, 360)
turtle.end_fill()

# 左眼
# 眼圈
turtle.penup()
turtle.goto(51, 82)
turtle.fillcolor("black")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(120)
turtle.circle(-32, 152)
turtle.circle(-100, 55)
turtle.circle(-25, 120)
turtle.circle(-120, 45)
turtle.end_fill()
# 眼珠

turtle.penup()
turtle.goto(79, 60)
turtle.fillcolor("white")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(0)
turtle.circle(24, 360)
turtle.end_fill()
turtle.penup()
turtle.goto(79, 64)
turtle.pencolor("darkslategray")
turtle.fillcolor("darkslategray")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(0)
turtle.circle(19, 360)
turtle.end_fill()
turtle.penup()
turtle.goto(79, 70)
turtle.fillcolor("black")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(0)
turtle.circle(10, 360)
turtle.end_fill()
turtle.penup()
turtle.goto(79, 88)
turtle.pencolor("white")
turtle.fillcolor("white")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(0)
turtle.circle(5, 360)
turtle.end_fill()

# 鼻子
turtle.penup()
turtle.goto(37, 80)
turtle.fillcolor("black")
turtle.begin_fill()
turtle.pendown()
turtle.circle(-8, 130)
turtle.circle(-22, 100)
turtle.circle(-8, 130)
turtle.end_fill()

# 嘴
turtle.penup()
turtle.goto(-15, 48)
turtle.setheading(-36)
turtle.begin_fill()
turtle.pendown()
turtle.circle(60, 70)
turtle.setheading(-132)
turtle.circle(-45, 100)
turtle.end_fill()

# 彩虹圈
turtle.penup()
turtle.goto(-135, 120)
turtle.pensize(5)
turtle.pencolor("cyan")
turtle.pendown()
turtle.setheading(60)
turtle.circle(-165, 150)
turtle.circle(-130, 78)
turtle.circle(-250, 30)
turtle.circle(-138, 105)
turtle.penup()
turtle.goto(-131, 116)
turtle.pencolor("slateblue")
turtle.pendown()
turtle.setheading(60)
turtle.circle(-160, 144)
turtle.circle(-120, 78)
turtle.circle(-242, 30)
turtle.circle(-135, 105)
turtle.penup()
turtle.goto(-127, 112)
turtle.pencolor("orangered")
turtle.pendown()
turtle.setheading(60)
turtle.circle(-155, 136)
turtle.circle(-116, 86)
turtle.circle(-220, 30)
turtle.circle(-134, 103)
turtle.penup()
turtle.goto(-123, 108)
turtle.pencolor("gold")
turtle.pendown()
turtle.setheading(60)
turtle.circle(-150, 136)
turtle.circle(-104, 86)
turtle.circle(-220, 30)
turtle.circle(-126, 102)
turtle.penup()
turtle.goto(-120, 104)
turtle.pencolor("greenyellow")
turtle.pendown()
turtle.setheading(60)
turtle.circle(-145, 136)
turtle.circle(-90, 83)
turtle.circle(-220, 30)
turtle.circle(-120, 100)
turtle.penup()

# 爱心
turtle.penup()
turtle.goto(220, 115)
turtle.pencolor("brown")
turtle.pensize(1)
turtle.fillcolor("brown")
turtle.begin_fill()
turtle.pendown()
turtle.setheading(36)
turtle.circle(-8, 180)
turtle.circle(-60, 24)
turtle.setheading(110)
turtle.circle(-60, 24)
turtle.circle(-8, 180)
turtle.end_fill()

# 五环
turtle.penup()
turtle.goto(-5, -170)
turtle.pendown()
turtle.pencolor("blue")
turtle.circle(6)
turtle.penup()
turtle.goto(10, -170)
turtle.pendown()
turtle.pencolor("black")
turtle.circle(6)
turtle.penup()
turtle.goto(25, -170)
turtle.pendown()
turtle.pencolor("brown")
turtle.circle(6)
turtle.penup()
turtle.goto(2, -175)
turtle.pendown()
turtle.pencolor("lightgoldenrod")
turtle.circle(6)
turtle.penup()
turtle.goto(16, -175)
turtle.pendown()
turtle.pencolor("green")
turtle.circle(6)
turtle.penup()
turtle.pencolor("black")
turtle.goto(-16, -160)
turtle.write("BEIJING 2022", font=('Arial', 10, 'bold italic'))
turtle.hideturtle()
turtle.done()
```

## ex2:模拟用户登陆

```python
user_list = ['root', 'admin']
passwd_list = ['root', '123']
i = 0
for i in range(3):
    user_input_name = input('Input user name:')

    # exit program
    if user_input_name == 'q':
        print('exit successfully')
        exit()

    # judge user_input_name exist user_list
    if user_input_name in user_list:

        user_input_passwd = input('Input password:')
        user_index = user_list.index(user_input_name)

        user_real_passwd = passwd_list[user_index]
        if user_input_passwd == user_real_passwd:
            print('login successfully!!!')
            break
        else:
            print('password is wrong，you have %d times chance' % (2 - i))
    else:
        print('user not exist ，you also have %d times chance' % (2 - i))
else:
    print('all times is gone，please login after hours')
```

## ex3:狐狸洞

```python
import random

day = 1
holeList = [0, 0, 0, 0, 0]
holeNum = random.randint(0, 4)
holeList[holeNum] = 1


def foxMove(holeList):
    move = random.randint(0, 1)
    if move == 1 and holeList[4] != 1:
        old = holeList.index(1)
        holeList[old] = 0
        holeList[old + 1] = 1
    else:
        old = holeList.index(1)
        holeList[old] = 0
        holeList[old - 1] = 1


while True:
    while True:
        try:
            playerNum = int(input("Please hole number(1,2,3,4,5):")) - 1
            if 0 <= playerNum <= 4:
                break
        except Exception:
            print("Please input again")
    if holeList[playerNum] == 1:
        print("Congratulate you in {} day {} number hole catch this fox".format(day, playerNum + 1))
        break
    else:
        print(playerNum + 1, "number hole no fox.")
        day += 1
        foxMove(holeList)

```

## ex4:三维向量及其运算


```python
class MyArray:
    def __init__(self,x,y,z):
        self.__x=x
        self.__y=y
        self.__z=z
    def add(self,a):
        x=self.__x+a.__x
        y=self.__y+a.__y
        z=self.__z+a.__z
        print("和=（{},{},{}）".format(x,y,z))
    def sub(self,a):
        x=self.__x-a.__x
        y=self.__y-a.__y
        z=self.__z-a.__z
        print("差=（{},{},{}）".format(x, y, z))
    def mul(self,a):
        x=self.__x*a
        y=self.__y*a
        z=self.__z*a
        print("乘积=（{},{},{}）".format(x, y, z))
    def truediv(self,a):
        x=self.__x/a
        y=self.__y/a
        z=self.__z/a
        print("商=（{},{},{}）".format(x, y, z))
    def length(self):
        a=pow(pow(self.__x,2)+pow(self.__y,2)+pow(self.__z,2),0.5)
        print("长度为：{}".format(round(a,3)))

print('请输入六个数a,b,c,d,e,f:')
a,b,c,d,e,f=map(int,input().split())
print('N1:',(a,b,c))
print('N2:',(d,e,f))
n1=MyArray(a,b,c)
n2=MyArray(d,e,f)
n1.add(n2)
n1.sub(n2)
n1.mul(2)
n1.truediv(2)
n1.length()
```

## ex5:密码强度和正则表达

```python
import re
patlower = '[a-z]+'
patupper = '[A-Z]+'
patdigit = '[0-9]+'
patchara = '[,./!;?<>]'


def checkpwd2(pwd2):
    d = {
    1:'弱密码', 2:'中低', 3:'中高', 4:'强密码'}
    r = [False] * 4
    for ch in pwd2:
        if bool(re.search(patlower, ch)):
            r[0] = True
        elif bool(re.search(patupper, ch)):
            r[1] = True
        elif bool(re.search(patdigit, ch)):
            r[2] = True
        elif bool(re.search(patchara, ch)):
            r[3] = True
    return d.get(r.count(True), 'error')

for i in range(4):
    pwd2 = input("请输入您的密码:")
    print(checkpwd2(pwd2))
```

```python
from re import findall

text = '''aabb, ccsd, 5566, iidi, 1234, 行尸走肉、金蝉脱壳、百里挑一、金玉满堂、
相亲相爱、八仙过海、金玉良缘、掌上明珠、皆大欢喜、
浩浩荡荡、平平安安、秀秀气气、斯斯文文、高高兴兴'''
print("全部词语： ", text)
# pattern = r'(((.).\3.)|((.)\5(.)\6))'
# pattern = r'((?P<f>.)(?P=f)(?P<g>.)(?P=g))'
pattern = r'(((.)\3(.)\4))'
print("匹配AABB的词语：", list(i[0] for i in findall(pattern, text)))
```

## ex6:计算机

```python
import random
import os
import tkinter
import tkinter.ttk
from docx import Document

columnsNumber = 4


def main(rowsNumbers=20, grade=4):
    if grade < 3:
        operators = '+-'
        biggest = 20
    elif grade <= 4:
        operators = '＋－×÷'
        biggest = 100
    elif grade == 5:
        operators = '＋－×÷('
        biggest = 100

    document = Document()
    table = document.add_table(rows=rowsNumbers, cols=columnsNumber)
    table.style.font.name = '宋体'
    for row in range(rowsNumbers):
        for col in range(columnsNumber):
            first = random.randint(1, biggest)
            second = random.randint(1, biggest)
            operator = random.choice(operators)

            if operator != '(':
                if operator == '-':
                    if first < second:
                        first, second = second, first
                    r = str(first).ljust(2, ' ') + ' ' + operator + str(second).ljust(2, ' ') + '='
            else:
                third = random.randint(1, 100)
                while True:
                    o1 = random.choice(operators)
                    o2 = random.choice(operators)
                    if o1 != '(' and o2 != '(':
                        break
                rr = random.randint(1, 100)
                if rr > 50:
                    if o2 == '-':
                        if second < third:
                            second, third = third, second
                    r = str(first).ljust(2, ' ') + o1 + '(' + str(second).ljust(2, ' ') + o2 + str(third).ljust(2,
                                                                                                                ' ') + ')='
                else:
                    if o1 == '-':
                        if first < second:
                            first, second = second, first
                        r = '(' + str(first).ljust(2, ' ') + o1 + str(second).ljust(2, ' ') + ')' + o2 + str(
                            third).ljust(2, ' ') + '='
        cell = table.cell(row, col)
        cell.text = r
    document.save('kousuan.docx')


if __name__ == '__main__':
    app = tkinter.Tk()
    app.title('KouSuan.zWrite')
    app['width'] = 300
    app['height'] = 150
    lableNumber = tkinter.Label(app, text='Number:', justify=tkinter.RIGHT, width=50)
    lableNumber.place(x=10, y=40, width=50, height=20)
    comboNumber = tkinter.ttk.Combobox(app, values=(100, 200, 300, 400, 500), width=50)
    comboNumber.place(x=70, y=40, width=50, height=20)

    labelGrade = tkinter.Label(app, text='Grade:', justify=tkinter.RIGHT, width=50)
    labelGrade.place(x=130, y=40, width=50, height=20)
    comboGrade = tkinter.ttk.Combobox(app, values=(1, 2, 3, 4, 5), width=50)
    comboGrade.place(x=200, y=40, width=50, height=20)


    def generate():
        number = int(comboNumber.get())
        grade = int(comboGrade.get())
        main(number, grade)


    buttonGenerate = tkinter.Button(app, text='＝', width=40, command=generate)
    buttonGenerate.place(x=130, y=90, width=40, height=30)

    app.mainloop()

```

## ex7:961位院士信息爬取

```python

import re
import os
import os.path
import time
from urllib.request import urlopen

dstDir = 'YuanShi'
if not os.path.isdir(dstDir):
    os.mkdir(dstDir)

startUrl = r'http://www.cae.cn/cae/html/main/col48/column_48_1.html'
with urlopen(startUrl) as fp:
    content = fp.read().decode()

# 提取并遍历每位大牛链接
pattern = r'<li class="name_list"><a href="(.+)" target="_blank">(.+)</a></li>'
result = re.findall(pattern, content)
for item in result:
    perUrl, name = item
    # 测试是否获取信息
    print(perUrl)
    # 这里根据初爬结果进行改进
    name = name.replace('<h3>', '').replace('</h3>', '')
    name = os.path.join(dstDir, name)
    perUrl = r'http://www.cae.cn/' + perUrl
    with urlopen(perUrl) as fp:
        content = fp.read().decode()

    # 抓取简介
    pattern = r'<p>(.+?)</p>'
    result = re.findall(pattern, content)  # 返回string中所有与pattern匹配的全部字符串,返回形式为数组。
    if result:
        intro = re.sub('(<a.+</a>)|(&ensp;)|(&nbsp);', '', '\n'.join(result))
        with open(name + '.txt', 'w', encoding='utf8') as fp:
            fp.write(intro)
```

## ex8:龟兔赛跑图

```python

import numpy as np
import matplotlib.pyplot as plt

t = np.arange(0, 120, 0.5)

rabbit = np.piecewise(t,
                      [t < 10, t > 110],
                      [lambda x: 15 * x, lambda x: 20 * (x - 110) + 150, lambda x: 150]
                      )
tortoise = 3 * t

plt.plot(t, tortoise, label='tortoise')
plt.plot(t, rabbit, label='rabbit')
plt.title('running', fontsize=24)
plt.xlabel('time/s', fontsize=18)
plt.ylabel('distance/m')
plt.show()
```
