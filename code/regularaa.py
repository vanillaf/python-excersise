#!/usr/bin/env python

from re import findall

text = '''aabb, ccsd, 5566, iidi, 1234, 行尸走肉、金蝉脱壳、百里挑一、金玉满堂、
相亲相爱、八仙过海、金玉良缘、掌上明珠、皆大欢喜、
浩浩荡荡、平平安安、秀秀气气、斯斯文文、高高兴兴'''
print("全部词语： ", text)
# pattern = r'(((.).\3.)|((.)\5(.)\6))'
# pattern = r'((?P<f>.)(?P=f)(?P<g>.)(?P=g))'
pattern = r'(((.)\3(.)\4))'
print("匹配AABB的词语：", list(i[0] for i in findall(pattern, text)))
