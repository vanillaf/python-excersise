class Fruits:
    """水果类"""
    def __init__(self, color):
        # 颜色属性
        self.__color = color

    def get_color(self):
        return self.__color

if __name__ == '__main__':
    apple = Fruits("red")
    watermelon = Fruits("green")
    orange = Fruits("orange")
    print("apple is {}".format(apple.get_color()))
    print("watermelon is {}".format(watermelon.get_color()))
    print("orange is {}".format(orange.get_color()))
