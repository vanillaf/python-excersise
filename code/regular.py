'''正则表达式版本'''
import re
patlower = '[a-z]+'
patupper = '[A-Z]+'
patdigit = '[0-9]+'
patchara = '[,./!;?<>]'


def checkpwd2(pwd2):
    d = {
    1:'弱密码', 2:'中低', 3:'中高', 4:'强密码'}
    r = [False] * 4
    for ch in pwd2:
        if bool(re.search(patlower, ch)):
            r[0] = True
        elif bool(re.search(patupper, ch)):
            r[1] = True
        elif bool(re.search(patdigit, ch)):
            r[2] = True
        elif bool(re.search(patchara, ch)):
            r[3] = True
    return d.get(r.count(True), 'error')

for i in range(4):
    pwd2 = input("请输入您的密码:")
    print(checkpwd2(pwd2))
