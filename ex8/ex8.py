import numpy as np
import matplotlib.pyplot as plt

t = np.arange(0, 120, 0.5)

rabbit = np.piecewise(t,
                      [t < 10, t > 110],
                      [lambda x: 15 * x, lambda x: 20 * (x - 110) + 150, lambda x: 150]
                      )
tortoise = 3 * t

plt.plot(t, tortoise, label='tortoise')
plt.plot(t, rabbit, label='rabbit')
plt.title('running', fontsize=24)
plt.xlabel('time/s', fontsize=18)
plt.ylabel('distance/m')
plt.show()
